package com.dk.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * 网管跨域过滤器配置类
 */
@Configuration
public class DkCorsConfiguration {
    @Bean
    public CorsWebFilter corsWebFilter(){
        //url基本跨域源设置
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        //新建跨域配置值对象
        CorsConfiguration config = new CorsConfiguration();
        //设置跨域具体实现
        config.addAllowedHeader("*");//任意请求头跨域
        config.addAllowedMethod("*");//任意请求跨域
        config.addAllowedOrigin("*");//任意来源跨域
        config.setAllowCredentials(true);//cookie允许跨域
        source.registerCorsConfiguration("/**",config);//注册跨域配置
        return new CorsWebFilter(source);
    }
}
