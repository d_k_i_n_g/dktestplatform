package com.dk.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient//服务注册发现
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//排除数据源配置
public class DkGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DkGatewayApplication.class, args);
    }

}
