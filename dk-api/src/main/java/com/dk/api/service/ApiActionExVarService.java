package com.dk.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.common.utils.PageUtils;
import com.dk.api.entity.ApiActionExVarEntity;

import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
public interface ApiActionExVarService extends IService<ApiActionExVarEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

