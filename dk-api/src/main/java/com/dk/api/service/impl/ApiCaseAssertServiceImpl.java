package com.dk.api.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.ApiCaseAssertDao;
import com.dk.api.entity.ApiCaseAssertEntity;
import com.dk.api.service.ApiCaseAssertService;


@Service("apiCaseAssertService")
public class ApiCaseAssertServiceImpl extends ServiceImpl<ApiCaseAssertDao, ApiCaseAssertEntity> implements ApiCaseAssertService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiCaseAssertEntity> page = this.page(
                new Query<ApiCaseAssertEntity>().getPage(params),
                new QueryWrapper<ApiCaseAssertEntity>()
        );

        return new PageUtils(page);
    }

}