package com.dk.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.api.entity.GroupEntity;
import com.dk.common.utils.PageUtils;
import com.dk.api.entity.ApiConfigInfoEntity;
import com.dk.common.utils.R;

import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
public interface ApiConfigInfoService extends IService<ApiConfigInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R saveApiInfo(ApiConfigInfoEntity apiConfigInfoEntity);

    R updateApiInfo(ApiConfigInfoEntity data);

    R queryApiInfoList(String apiName, String path, Integer apiType, Integer method, Integer page, Integer limit);
}

