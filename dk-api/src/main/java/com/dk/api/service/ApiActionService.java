package com.dk.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.api.entity.dto.ApiActionDTO;
import com.dk.api.entity.vo.ApiActionVO;
import com.dk.common.utils.PageUtils;
import com.dk.api.entity.ApiActionEntity;
import com.dk.common.utils.R;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
public interface ApiActionService extends IService<ApiActionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R queryActionListByParams(ApiActionDTO data);

    R saveActionInfo(ApiActionVO data);

    R getActionInfoById(Integer actionId);

    R updateActionInfo(ApiActionVO data);

    R deleteActionInfoByIds(List<Integer> data);
}

