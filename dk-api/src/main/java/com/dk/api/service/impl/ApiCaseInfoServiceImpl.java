package com.dk.api.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dk.api.entity.dto.ApiCaseInfoDTO;
import com.dk.common.utils.Constant;
import com.dk.common.utils.R;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.ApiCaseInfoDao;
import com.dk.api.entity.ApiCaseInfoEntity;
import com.dk.api.service.ApiCaseInfoService;


@Service("apiCaseInfoService")
public class ApiCaseInfoServiceImpl extends ServiceImpl<ApiCaseInfoDao, ApiCaseInfoEntity> implements ApiCaseInfoService {
    @Autowired
    ApiCaseInfoDao apiCaseInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiCaseInfoEntity> page = this.page(
                new Query<ApiCaseInfoEntity>().getPage(params),
                new QueryWrapper<ApiCaseInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据参数查询用例列表
     * @param data
     * @return
     */
    @Override
    public R queryCaseListByParams(ApiCaseInfoDTO data) {
        Page<ApiCaseInfoEntity> page = new Page<>(data.getPage(),data.getLimit());
        QueryWrapper<ApiCaseInfoEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(data.getCaseId()!=0,"case_id",data.getCaseId())
                .eq(data.getApiId()!=0,"api_id",data.getApiId())
                .like(Strings.isNotBlank(data.getCaseName()),"case_name",data.getCaseName())
                .eq(data.getBodyType()!=0,"body_type",data.getBodyType())
                .orderByDesc("create_time");
        IPage<ApiCaseInfoEntity> iPage = apiCaseInfoDao.selectPage(page,queryWrapper);
        return R.ok().put(Constant.PAGE,new PageUtils(iPage));
    }

}