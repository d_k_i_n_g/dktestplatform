package com.dk.api.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.ApiActionExVarDao;
import com.dk.api.entity.ApiActionExVarEntity;
import com.dk.api.service.ApiActionExVarService;


@Service("apiActionExVarService")
public class ApiActionExVarServiceImpl extends ServiceImpl<ApiActionExVarDao, ApiActionExVarEntity> implements ApiActionExVarService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiActionExVarEntity> page = this.page(
                new Query<ApiActionExVarEntity>().getPage(params),
                new QueryWrapper<ApiActionExVarEntity>()
        );

        return new PageUtils(page);
    }

}