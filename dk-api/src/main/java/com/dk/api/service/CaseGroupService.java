package com.dk.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.common.utils.PageUtils;
import com.dk.api.entity.CaseGroupEntity;

import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
public interface CaseGroupService extends IService<CaseGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

