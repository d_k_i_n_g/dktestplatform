package com.dk.api.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.ApiCaseActionDao;
import com.dk.api.entity.ApiCaseActionEntity;
import com.dk.api.service.ApiCaseActionService;


@Service("apiCaseActionService")
public class ApiCaseActionServiceImpl extends ServiceImpl<ApiCaseActionDao, ApiCaseActionEntity> implements ApiCaseActionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiCaseActionEntity> page = this.page(
                new Query<ApiCaseActionEntity>().getPage(params),
                new QueryWrapper<ApiCaseActionEntity>()
        );

        return new PageUtils(page);
    }

}