package com.dk.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.api.entity.dto.GroupDTO;
import com.dk.common.utils.PageUtils;
import com.dk.api.entity.GroupEntity;
import com.dk.common.utils.R;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
public interface GroupService extends IService<GroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<GroupEntity> getGroupList(@Valid GroupDTO data);

    void deleteGroupInId(List<Integer> data);

    R addCaseGroup(GroupEntity data);

    R updateCaseGroupById(GroupEntity data);
}

