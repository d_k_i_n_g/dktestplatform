package com.dk.api.service.impl;

import com.dk.api.entity.dto.GroupDTO;
import com.dk.api.feign.SysUserService;
import com.dk.common.utils.R;
import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.GroupDao;
import com.dk.api.entity.GroupEntity;
import com.dk.api.service.GroupService;


@Service("groupService")
public class GroupServiceImpl extends ServiceImpl<GroupDao, GroupEntity> implements GroupService {
    @Autowired
    GroupDao groupDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<GroupEntity> page = this.page(
                new Query<GroupEntity>().getPage(params),
                new QueryWrapper<GroupEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据参数查询接口列表
     * @param groupDTO
     * @return
     */
    @Override
    public List<GroupEntity> getGroupList(GroupDTO groupDTO) {
        return groupDao.selectList(new QueryWrapper<GroupEntity>()
                .eq(StringUtils.isNotBlank(groupDTO.getGroupId().toString())&&groupDTO.getGroupId()!=0,"group_id",groupDTO.getGroupId())
                .eq(StringUtils.isNotBlank(groupDTO.getGroupName()),"group_name",groupDTO.getGroupName()));
    }

    /**
     * 根据传入的id数组批量删除数据
     * @param ids 数组列表
     */
    @Override
    public void deleteGroupInId(List<Integer> ids) {
        ids.stream().forEach(groupId->{
            if(groupDao.exists(new QueryWrapper<GroupEntity>().eq("group_id",groupId))){
                groupDao.deleteById(groupId);
            }
        });
    }

    /**
     * 添加组信息
     * @param groupEntity 组信息
     */
    @Override
    public R addCaseGroup(GroupEntity groupEntity) {
        if(groupDao.exists(new QueryWrapper<GroupEntity>().eq("group_name",groupEntity.getGroupName()))){
            return R.error(400,"当前组名已存在，请重新输入组名");
        }else{
            groupDao.insert(groupEntity);
            return R.ok();
        }
    }

    /**
     * 根据id修改组信息
     * @param groupEntity 组信息
     * @return 修改结果
     */
    @Override
    public R updateCaseGroupById(GroupEntity groupEntity) {
        if(groupDao.exists(new QueryWrapper<GroupEntity>().eq("group_name",groupEntity.getGroupName())
                .ne("group_id",groupEntity.getGroupId())))
        {
            return R.error(400,"当前组名已存在，请重新输入组名");
        }else if(!groupDao.exists(new QueryWrapper<GroupEntity>().eq("group_id",groupEntity.getGroupId()))){
            return R.error(400,"当前用例组id不存在，请检查请求数据");
        } else{
            groupDao.updateById(groupEntity);
            return R.ok();
        }
    }

}