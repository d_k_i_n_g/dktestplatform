package com.dk.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.api.entity.dto.ApiCaseInfoDTO;
import com.dk.common.utils.PageUtils;
import com.dk.api.entity.ApiCaseInfoEntity;
import com.dk.common.utils.R;

import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
public interface ApiCaseInfoService extends IService<ApiCaseInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R queryCaseListByParams(ApiCaseInfoDTO data);
}

