package com.dk.api.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.CaseGroupDao;
import com.dk.api.entity.CaseGroupEntity;
import com.dk.api.service.CaseGroupService;


@Service("caseGroupService")
public class CaseGroupServiceImpl extends ServiceImpl<CaseGroupDao, CaseGroupEntity> implements CaseGroupService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CaseGroupEntity> page = this.page(
                new Query<CaseGroupEntity>().getPage(params),
                new QueryWrapper<CaseGroupEntity>()
        );

        return new PageUtils(page);
    }

}