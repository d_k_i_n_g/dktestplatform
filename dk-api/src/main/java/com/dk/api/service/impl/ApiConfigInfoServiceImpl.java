package com.dk.api.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dk.api.entity.GroupEntity;
import com.dk.common.utils.Constant;
import com.dk.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.api.dao.ApiConfigInfoDao;
import com.dk.api.entity.ApiConfigInfoEntity;
import com.dk.api.service.ApiConfigInfoService;


@Service("apiConfigInfoService")
public class ApiConfigInfoServiceImpl extends ServiceImpl<ApiConfigInfoDao, ApiConfigInfoEntity> implements ApiConfigInfoService {
    @Autowired
    ApiConfigInfoDao apiConfigInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiConfigInfoEntity> page = this.page(
                new Query<ApiConfigInfoEntity>().getPage(params),
                new QueryWrapper<ApiConfigInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public R queryApiInfoList(String apiName, String path, Integer apiType, Integer method, Integer page, Integer limit) {
        Page<ApiConfigInfoEntity> pageObJ = new Page<>(page,limit);
        QueryWrapper<ApiConfigInfoEntity> queryWrapper = new QueryWrapper<ApiConfigInfoEntity>();
        queryWrapper.like(apiName!=null&&apiName instanceof String,"api_name",apiName)
                .eq(apiType!=null&&apiType instanceof Integer,"api_type",apiType)
                .like(path!=null&&path instanceof String,"path",path)
                .eq(method!=null&&method instanceof Integer,"method",method)
                .orderByDesc("create_time");
        IPage<ApiConfigInfoEntity> iPage = apiConfigInfoDao.selectPage(pageObJ,queryWrapper);
        return R.ok().put("page",new PageUtils(iPage));
    }

    /**
     * 保存接口信息
     * @param apiConfigInfoEntity 接口信息对象
     * @return 结果
     */
    @Override
    public R saveApiInfo(ApiConfigInfoEntity apiConfigInfoEntity) {
        if(apiConfigInfoDao.exists(new QueryWrapper<ApiConfigInfoEntity>().eq("api_name",apiConfigInfoEntity.getApiName()))){
            return R.error(400,"接口名称已存在,请重新输入");
        }else{
            apiConfigInfoDao.insert(apiConfigInfoEntity);
            return R.ok("添加接口信息成功");
        }
    }

    /**
     * 更新接口信息
     * @param apiConfigInfoEntity 接口信息对象
     * @return 结果
     */
    @Override
    public R updateApiInfo(ApiConfigInfoEntity apiConfigInfoEntity) {
        if(apiConfigInfoDao.exists(new QueryWrapper<ApiConfigInfoEntity>()
                .eq("api_name",apiConfigInfoEntity.getApiName())
                .ne("api_id",apiConfigInfoEntity.getApiId())))
        {
            return R.error(400,"接口名称已存在,请重新输入");
        }else{
            apiConfigInfoDao.updateById(apiConfigInfoEntity);
            return R.ok("添加接口信息成功");
        }
    }



}