package com.dk.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableFeignClients(basePackages="com.dk.api.feign")//启用远程调用，指定调用包
@MapperScan("com.dk.api.dao")//扫描dao层
@SpringBootApplication//获取+自动配置
@EnableDiscoveryClient//服务发现注解
@EnableSwagger2//swagger扫描注解
@EnableTransactionManagement//开启事务
public class DkApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(DkApiApplication.class, args);
    }
}
