package com.dk.api.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis-plus插件
 */
@Configuration
@MapperScan("com.dk.api.dao")
//添加扫描注解，写入mapper的路径，否则会找不到接口并报错
public class MybatisPlusConfig {
    //注册mybatis拦截器
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());//乐观锁插件
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());//分页插件
        return interceptor;
    }
}
