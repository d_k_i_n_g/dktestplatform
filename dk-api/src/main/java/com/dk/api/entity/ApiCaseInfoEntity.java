package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.dk.common.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Data
@TableName("dk_api_case_info")
public class ApiCaseInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用例主键
	 */
	@ApiModelProperty(value = "用例主键",required = false,position = 1,example = "1")
	@TableField("case_id")
	@TableId
	private Integer caseId;

	/**
	 * 接口主键
	 */
	@ApiModelProperty(value = "接口主键",required = false,position = 2,example = "1")
	@TableField("api_id")
	private Integer apiId;

	/**
	 * 用例名
	 */
	@Size(max = 20,message = "caseName不得超过20位")
	@NotBlank(message = "caseName不得为空")
	@ApiModelProperty(value = "用例名",required = true,position = 3,example = "用例名")
	@TableField("case_name")
	private String caseName;

	/**
	 * 用例请求头
	 */
	@Size(max = 200,message = "caseHeader不得超过200位")
	@NotBlank(message = "caseHeader不得为空")
	@ApiModelProperty(value = "用例请求头",required = true,position = 4,example = "用例请求头")
	@TableField("case_header")
	private String caseHeader;

	/**
	 * 请求体类型(1:json,2:xml,3:formdata)
	 */
	@Max(value = 3,message = "body_type不得超过3")
	@NotNull(message = "body_type不得为空")
	@EnumValue(intValues = {1,2,3},message = "body_type不正确(枚举值 1:json,2:xml,3:formdata)")
	@ApiModelProperty(value = "请求体类型",required = true,position = 5,example = "1")
	@TableField("body_type")
	private Integer bodyType;

	/**
	 * 请求主体
	 */
	@Size(max = 65535,message = "body不得超过65535位")
	@NotBlank(message = "body不得为空")
	@ApiModelProperty(value = "请求主体",required = true,position = 6,example = "{json:'json'}")
	@TableField("body")
	private String body;

	/**
	 * 备注
	 */
	@Size(max = 50,message = "备注不得超过50位")
	@TableField("remark")
	@ApiModelProperty(value = "备注",required = false,position = 7,example = "备注信息")
	private String remark;

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	@TableLogic
	@ApiModelProperty(value = "是否删除",required = false,position = 8,example = "0")
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间",position = 9,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;

	/**
	 * 创建人id
	 */
	@ApiModelProperty(value = "创建人id",required = false,position = 10,example = "12345678")
	@TableField("create_user_id")
	private Long createUserId;

	/**
	 * 编辑时间
	 */
	@ApiModelProperty(value = "创建时间",position = 11,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

	/**
	 * 编辑人id
	 */
	@ApiModelProperty(value = "编辑人id",required = false,position = 12,example = "12345678")
	@TableField("update_user_id")
	private Long updateUserId;

}
