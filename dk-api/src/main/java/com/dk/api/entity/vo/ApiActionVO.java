package com.dk.api.entity.vo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.dk.api.entity.ApiActionExVarEntity;
import com.dk.common.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * 接口动作实体类
 */
@Data
public class ApiActionVO {
    private static final long serialVersionUID = 1L;

    /**
     * 动作主键
     */
    @ApiModelProperty(value = "动作主键",required = false,position = 1,example = "1")
    private Integer actionId;

    /**
     * 动作名称
     */
    @Size(max = 20,message = "动作名称不得超过20位")
    @NotBlank(message = "动作名称不得为空")
    @ApiModelProperty(value = "动作名称",required = true,position = 2,example = "动作名称")
    private String actionName;

    /**
     * 动作类型
     */
    @EnumValue(intValues = {1,2,3,4,5,6},message = "枚举值错误(枚举 1:用例型动作,2:db_select,3:db_insert,4:db_update,5:db_delete,6:时间等待型动作)")
    @NotNull(message = "动作类型不得为空")
    @ApiModelProperty(value = "动作类型",required = true,position = 3,example = "1")
    private Integer actionType;

    /**
     * 操作用例id
     */
    @Max(value = 99999999,message = "操作用例id不得超过8位")
    @ApiModelProperty(value = "操作用例id",required = false,position = 4,example = "1")
    private Integer handleCaseId;

    /**
     * 操作用例名称
     */
    @Size(max = 100,message = "操作用例名称不得超过100位")
    @ApiModelProperty(value = "操作用例名称",required = false,position = 5,example = "操作用例名称")
    private String handleCaseName;

    /**
     * 操作数据库id
     */
    @Max(value = 99999999,message = "操作数据库id不得超过8位")
    @ApiModelProperty(value = "操作数据库id",required = false,position = 6,example = "1")
    private Integer handleDbId;

    /**
     * 操作数据库名称
     */
    @Size(max = 100,message = "操作数据库名称不得超过100位")
    @ApiModelProperty(value = "操作数据库名称",required = false,position = 7,example = "操作数据库名称")
    private String handleDbName;

    /**
     * 操作sql语句
     */
    @Size(max = 2000,message = "操作sql语句不得超过2000位")
    @ApiModelProperty(value = "操作sql语句",required = false,position = 8,example = "select * from User")
    private String handleSql;

    /**
     * 等待时间
     */
    @Max(value = 10000,message = "等待时间不不得超过10000ms")
    @ApiModelProperty(value = "等待时间",required = false,position = 9,example = "100")
    private Integer handleWaitTime;

    /**
     * 是否删除（0：未删除，1：已删除）
     */
    @TableLogic
    @ApiModelProperty(value = "isDelete",required = false,position = 10,example = "1")
    private Integer isDelete;

    /**
     * 动作提取参数集合
     */
    @ApiModelProperty(value = "动作提取参数集合",required = false,position = 11,example = "List")
    private List<ApiActionExVarEntity> apiActionExVarList;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "createTime",required = false,position = 12,example = "2022-01-01 01:01:01")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "createUserId",required = false,position = 13,example = "123456789")
    private Long createUserId;

    /**
     * 编辑时间
     */
    @ApiModelProperty(value = "updateTime",required = false,position = 14,example = "2022-01-01 01:01:01")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
    /**
     * 编辑人id
     */
    @ApiModelProperty(value = "updateUserId",required = false,position = 15,example = "1")
    private Long updateUserId;
}
