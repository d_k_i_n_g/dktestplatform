package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.dk.common.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;

/**
 * 接口配置信息类
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Data
@TableName("dk_api_config_info")
public class ApiConfigInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 接口主键
	 */
	@TableId
	@TableField("api_id")
	@ApiModelProperty(value = "接口Id",required = false,position = 1,example = "1")
	private Integer apiId;

	/**
	 * 接口名称
	 */
	@Size(max = 15,message = "接口名称不得超过15位")
	@NotBlank(message = "接口名称不得为空")
	@TableField("api_name")
	@ApiModelProperty(value = "接口名称",required = true,position = 2,example = "接口名称")
	private String apiName;

	/**
	 * 接口类型（1：restful，2：rpc，3：webservice）
	 */
	@NotNull(message = "接口类型不得为空")
	@DecimalMax(value = "100000000",message = "apiType不得超过8位")
	@EnumValue(intValues = {1,2,3},message = "apiType不正确(枚举值 1：restful，2：rpc，3：webservice)")
	@TableField("api_type")
	@ApiModelProperty(value = "接口类型",required = true,position = 3,example = "1")
	private Integer apiType;

	/**
	 * 请求方式（1:get,2:post,3:delete,4:update）
	 */
	@NotNull(message = "请求方式不得为空")
	@DecimalMax(value = "100000000",message = "method不得超过8位")
	@EnumValue(intValues = {1,2,3,4},message = "apiType不正确(枚举值 1:get,2:post,3:delete,4:update)")
	@TableField("method")
	@ApiModelProperty(value = "请求方式",required = true,position = 4,example = "1")
	private Integer method;

	/**
	 * 请求路径
	 */
	@NotBlank(message = "请求路径不得为空")
	@Size(max = 100,message = "请求路径不得超过100位")
	@Pattern(regexp = "(\\/([0-9a-zA-Z]+))+",message = "请求路径格式错误,请检查数据")
	@TableField("path")
	@ApiModelProperty(value = "请求路径",required = true,position = 5,example = "/path")
	private String path;

	/**
	 * 备注
	 */
	@Size(max = 50,message = "备注不得超过50位")
	@TableField("remark")
	@ApiModelProperty(value = "备注",required = false,position = 6,example = "备注信息")
	private String remark;

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	@TableLogic
	@ApiModelProperty(value = "是否删除",required = false,position = 7,example = "0")
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间",required = false,position = 8,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@TableField(value = "create_time",fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 创建人id
	 */
	@ApiModelProperty(value = "创建人id",required = false,position = 9,example = "12345678")
	@TableField("create_user_id")
	private Long createUserId;
	/**
	 * 编辑时间
	 */
	@ApiModelProperty(value = "创建时间",required = false,position = 10,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	/**
	 * 编辑人id
	 */
	@ApiModelProperty(value = "编辑人id",required = false,position = 9,example = "12345678")
	@TableField("update_user_id")
	private Long updateUserId;

}
