package com.dk.api.entity.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;
/**
 *
 * 组信息
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
@Data
public class GroupDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 接口组主键
     */
    @TableId
    private Integer groupId;
    /**
     * 组名
     */
    private String groupName;

}
