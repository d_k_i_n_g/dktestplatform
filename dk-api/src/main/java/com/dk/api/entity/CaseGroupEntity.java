package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
@Data
@TableName("dk_api_case_group")
public class CaseGroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 用例id
	 */
	private Integer caseId;
	/**
	 * 组id
	 */
	private Integer groupId;
	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	private Integer isDelete;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人id
	 */
	private Long createUserId;
	/**
	 * 编辑时间
	 */
	private Date updateTime;
	/**
	 * 编辑人id
	 */
	private Long updateUserId;

}
