package com.dk.api.entity.dto;

import com.dk.common.annotation.EnumValue;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 动作查询实体类
 */
@Data
public class ApiActionDTO {
    /**
     * 动作名称
     */
    @Size(max = 20,message = "动作名称不得超过20位")
    @ApiModelProperty(value = "动作名称",required = true,position = 1,example = "动作名称")
    private String actionName;

    /**
     * 动作类型
     */
    @Max(value = 7,message = "动作类型必须小于7")
    @ApiModelProperty(value = "动作类型",required = true,position = 2,example = "1")
    private Integer actionType;

    /**
     * 页码
     */
    @Max(value = 9999,message = "页码最多9999页")
    @NotNull(message = "page不得为空")
    @ApiModelProperty(value = "页码",required = true,position = 3,example = "1")
    private Integer page;

    /**
     * 查询数据量
     */
    @Max(value = 100,message = "页面数据量最多100条")
    @NotNull(message = "limit不得为空")
    @ApiModelProperty(value = "查询数据量",required = true,position = 4,example = "10")
    private Integer limit;
}
