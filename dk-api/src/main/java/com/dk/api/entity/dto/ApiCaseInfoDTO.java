package com.dk.api.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dk.common.annotation.EnumValue;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Data
public class ApiCaseInfoDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 用例主键
     */
    @ApiModelProperty(value = "用例主键",required = false,position = 1,example = "1")
    @TableField("case_id")
    @TableId
    private Integer caseId;

    /**
     * 接口主键
     */
    @ApiModelProperty(value = "接口主键",required = false,position = 2,example = "1")
    @TableField("api_id")
    private Integer apiId;

    /**
     * 用例名
     */
    @Size(max = 20,message = "caseName不得超过20位")
    @ApiModelProperty(value = "用例名",required = true,position = 3,example = "用例名")
    @TableField("case_name")
    private String caseName;

    /**
     * 请求体类型(1:json,2:xml,3:formdata)
     */
    @Max(value = 3,message = "body_type不得超过3")
    @ApiModelProperty(value = "请求体类型",required = true,position = 4,example = "1")
    @TableField("body_type")
    private Integer bodyType;


    /**
     * 页码
     */
    @Max(value = 9999,message = "页码最多9999页")
    @NotNull(message = "page不得为空")
    @ApiModelProperty(value = "页码",required = true,position = 5,example = "1")
    private Integer page;

    /**
     * 查询数据量
     */
    @Max(value = 100,message = "页面数据量最多100条")
    @NotNull(message = "limit不得为空")
    @ApiModelProperty(value = "查询数据量",required = true,position = 6,example = "10")
    private Integer limit;
}
