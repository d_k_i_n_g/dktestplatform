package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Data
@TableName("dk_api_case_assert")
public class ApiCaseAssertEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 断言主键
	 */
	@TableId
	private Integer assertId;
	/**
	 * 接口id
	 */
	private Integer caseId;
	/**
	 * 断言类型
	 */
	private Integer assertType;
	/**
	 * 检查位置
	 */
	private Integer checkMain;
	/**
	 * 校验类型
	 */
	private Integer checkType;
	/**
	 * 校验公式
	 */
	private String checkEquation;
	/**
	 * 校验数据库id
	 */
	private Integer checkDbId;
	/**
	 * 目标结果
	 */
	private String result;
	/**
	 * 校验查询语句
	 */
	private String checkSql;
	/**
	 * 查询结果下标
	 */
	private String checkSqlSub;
	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	private Integer isDelete;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人id
	 */
	private Long createUserId;
	/**
	 * 编辑时间
	 */
	private Date updateTime;
	/**
	 * 编辑人id
	 */
	private Long updateUserId;

}
