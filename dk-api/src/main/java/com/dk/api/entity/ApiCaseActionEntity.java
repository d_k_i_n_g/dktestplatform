package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Data
@TableName("dk_api_case_action")
public class ApiCaseActionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 关联主键
	 */
	@TableId
	private Integer id;
	/**
	 * 用例id
	 */
	private Integer caseId;
	/**
	 * 动作id
	 */
	private Integer actionId;
	/**
	 * 动作类型（1：前置，2：后置）
	 */
	private Integer actionType;
	/**
	 * 执行顺序
	 */
	private Integer exSub;

}
