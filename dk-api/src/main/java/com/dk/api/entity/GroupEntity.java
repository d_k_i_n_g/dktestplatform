package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
@Data
@TableName("dk_api_group")
public class GroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 接口组主键
	 */
	@TableId
	@TableField("group_id")
	@ApiModelProperty(value = "groupId",required = false,position = 1,example = "1")
	private Integer groupId;
	/**
	 * 组名
	 */
	@TableField("group_name")
	@ApiModelProperty(value = "groupName",required = true,position = 2,example = "测试组")
	@Size(max = 10,message = "组名称不得超过10位")
	@NotBlank(message = "组名称不能为空")
	private String groupName;

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	@TableLogic
	@ApiModelProperty(value = "isDelete",required = false,position = 3,example = "1")
	private Integer isDelete;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time",fill = FieldFill.INSERT)
	@ApiModelProperty(value = "createTime",required = false,position = 4,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;
	/**
	 * 创建人id
	 */
	@TableField("create_user_id")
	@ApiModelProperty(value = "createUserId",required = false,position = 5,example = "123456789")
	private Long createUserId;
	/**
	 * 编辑时间
	 */
	@TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
	@ApiModelProperty(value = "updateTime",required = false,position = 6,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;
	/**
	 * 编辑人id
	 */
	@TableField("update_user_id")
	@ApiModelProperty(value = "updateUserId",required = false,position = 7,example = "1")
	private Long updateUserId;

}
