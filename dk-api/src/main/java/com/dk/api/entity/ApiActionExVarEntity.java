package com.dk.api.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.dk.common.annotation.EnumValue;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Data
@TableName("dk_api_action_ex_var")
public class ApiActionExVarEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 提取参数主键
	 */
	@ApiModelProperty(value = "提取参数主键",required = false,position = 1,example = "1")
	@TableField("ex_var_id")
	@TableId
	private Integer exVarId;

	/**
	 * 动作id
	 */
	@Max(value = 99999999,message = "动作主键不得超过8位")
	@NotNull(message = "依赖的动作主键不得为空")
	@ApiModelProperty(value = "动作主键",required = true,position = 2,example = "1")
	@TableField("action_id")
	private Integer actionId;

	/**
	 * 动作类型
	 */
	@EnumValue(intValues = {1,2,3},message = "枚举值错误(枚举 1:用例型动作,2:sql型动作,3:时间等待型动作)")
	@NotNull(message = "动作类型不得为空")
	@ApiModelProperty(value = "动作类型",required = true,position = 3,example = "1")
	@TableField("action_type")
	private Integer actionType;

	/**
	 * 数据库结果提取下标
	 */
	@Max(value = 99999999,message = "数据库结果提取下标不得超过8位")
	@ApiModelProperty(value = "数据库结果提取下标",required = false,position = 4,example = "1")
	@TableField("db_ex_sub")
		private Integer dbExSub;

	/**
	 * 提取字段
	 */
	@Size(max = 20,message = "数据库提取字段不得超过20位")
	@ApiModelProperty(value = "数据库提取字段",required = false,position = 5,example = "数据库提取字段")
	@TableField("db_ex_field")
	private String dbExField;

	/**
	 * 用例提取位置
	 */
	@Max(value = 99999999,message = "用例提取位置不得超过8位")
	@EnumValue(intValues = {1,2},message = "枚举值错误(枚举 1.响应头,2.响应体)")
	@ApiModelProperty(value = "用例提取位置",required = false,position = 6,example = "caseExEquation")
	@TableField("case_ex_position")
	private Integer caseExPosition;

	/**
	 * 用例提取公式
	 */
	@Size(max = 50,message = "用例结果提取公式不得超过50位")
	@ApiModelProperty(value = "用例结果提取公式",required = false,position = 7,example = "$.data.userName")
	@TableField("case_ex_equation")
	private String caseExEquation;

	/**
	 * 存储key值
	 */
	@Size(max = 50,message = "存储key值不得超过50位")
	@ApiModelProperty(value = "存储key值",required = false,position = 8,example = "userName")
	@TableField("set_key")
	private String setKey;

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	@TableLogic
	@ApiModelProperty(value = "isDelete",required = false,position = 9,example = "0")
	private Integer isDelete;

	@Override
	public boolean equals(Object obj) {
		// 首先判断传进来的obj是否是调用equals方法对象的this本身，提高判断效率
		if (obj == this) {return true;}
		// 判断传进来的obj是否是null，提高判断效率
		if (obj == null) {return false;}
		// 判断传进来的obj是否是ApiActionExVarEntity对象，防止出现类型转换的异常
		if (obj instanceof ApiActionExVarEntity) {
			ApiActionExVarEntity apiActionExVarEntity = (ApiActionExVarEntity) obj;
			System.out.println("测试:!!!!!!!!!!"+this.exVarId.toString()+","+apiActionExVarEntity.exVarId.toString());
			boolean flag = this.exVarId.toString().equals(apiActionExVarEntity.exVarId.toString());
			return flag;
		}
		// 如果没有走类型判断语句说明两个比较的对象它们的类型都不一样，结果就是false了
		return false;
	}
}
