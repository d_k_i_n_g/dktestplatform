package com.dk.api.feign;

import com.dk.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("dk-config")//指定为远程调用客户端服务，指定需要调用的服务名称
public interface EnvironmentInfoService {
    @RequestMapping("/config/environmentinfo/info/{id}")
    R info(@PathVariable("id") Integer id);
}
