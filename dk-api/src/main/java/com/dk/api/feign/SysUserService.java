package com.dk.api.feign;

import com.dk.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("renren-fast")
public interface SysUserService {
    /**
     * 获取登录的用户信息
     */
    @GetMapping("/renren-fast/sys/user/info")
    String info();
}
