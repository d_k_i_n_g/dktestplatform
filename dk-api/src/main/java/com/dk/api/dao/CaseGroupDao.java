package com.dk.api.dao;

import com.dk.api.entity.CaseGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
@Mapper
public interface CaseGroupDao extends BaseMapper<CaseGroupEntity> {
	
}
