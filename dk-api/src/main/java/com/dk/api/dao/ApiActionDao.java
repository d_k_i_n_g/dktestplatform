package com.dk.api.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dk.api.entity.ApiActionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dk.api.entity.ApiActionExVarEntity;
import com.dk.api.entity.dto.ApiActionDTO;
import com.dk.api.entity.vo.ApiActionVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Mapper
public interface ApiActionDao extends BaseMapper<ApiActionEntity> {
    IPage<ApiActionVO> queryActionList(IPage<ApiActionEntity> page, @Param("apiActionDTO") ApiActionDTO apiActionDTO);

    Integer saveApiActionExVarList(List<ApiActionExVarEntity> apiActionExVarList);

    ApiActionVO selectActionInfoById(@Param("actionId") Integer actionId);
}
