package com.dk.api.dao;

import com.dk.api.entity.ApiConfigInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Mapper
public interface ApiConfigInfoDao extends BaseMapper<ApiConfigInfoEntity> {
	
}
