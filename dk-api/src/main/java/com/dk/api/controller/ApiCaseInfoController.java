package com.dk.api.controller;

import java.util.Arrays;
import java.util.Map;

import com.dk.api.entity.dto.ApiCaseInfoDTO;
import com.dk.common.utils.BaseReqDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.api.entity.ApiCaseInfoEntity;
import com.dk.api.service.ApiCaseInfoService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;


@RestController
@RequestMapping("api/apicaseinfo")
public class ApiCaseInfoController {
    @Autowired
    private ApiCaseInfoService apiCaseInfoService;

    @PostMapping("/queryCaseListByParams")
    //@ApiOperation(value = "根据参数查询用例列表",notes = "根据参数查询用例列表")
    public R queryCaseListByParams(@Validated @RequestBody BaseReqDTO<ApiCaseInfoDTO> req){
        return apiCaseInfoService.queryCaseListByParams(req.getData());
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("api:apicaseinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = apiCaseInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{caseId}")
    //@RequiresPermissions("api:apicaseinfo:info")
    public R info(@PathVariable("caseId") Integer caseId){
		ApiCaseInfoEntity apiCaseInfo = apiCaseInfoService.getById(caseId);

        return R.ok().put("apiCaseInfo", apiCaseInfo);
    }

//    /**
//     * 保存
//     */
//    @RequestMapping("/save")
//    //@RequiresPermissions("api:apicaseinfo:save")
//    public R save(@RequestBody ApiCaseInfoEntity apiCaseInfo){
//		apiCaseInfoService.save(apiCaseInfo);
//
//        return R.ok();
//    }
//
//    /**
//     * 修改
//     */
//    @RequestMapping("/update")
//    //@RequiresPermissions("api:apicaseinfo:update")
//    public R update(@RequestBody ApiCaseInfoEntity apiCaseInfo){
//		apiCaseInfoService.updateById(apiCaseInfo);
//
//        return R.ok();
//    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("api:apicaseinfo:delete")
    public R delete(@RequestBody Integer[] caseIds){
		apiCaseInfoService.removeByIds(Arrays.asList(caseIds));

        return R.ok();
    }

}
