package com.dk.api.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dk.api.entity.ApiCaseAssertEntity;
import com.dk.api.service.ApiCaseAssertService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;



/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@RestController
@RequestMapping("api/apicaseassert")
public class ApiCaseAssertController {
    @Autowired
    private ApiCaseAssertService apiCaseAssertService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("api:apicaseassert:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = apiCaseAssertService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{assertId}")
    //@RequiresPermissions("api:apicaseassert:info")
    public R info(@PathVariable("assertId") Integer assertId){
		ApiCaseAssertEntity apiCaseAssert = apiCaseAssertService.getById(assertId);

        return R.ok().put("apiCaseAssert", apiCaseAssert);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("api:apicaseassert:save")
    public R save(@RequestBody ApiCaseAssertEntity apiCaseAssert){
		apiCaseAssertService.save(apiCaseAssert);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("api:apicaseassert:update")
    public R update(@RequestBody ApiCaseAssertEntity apiCaseAssert){
		apiCaseAssertService.updateById(apiCaseAssert);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("api:apicaseassert:delete")
    public R delete(@RequestBody Integer[] assertIds){
		apiCaseAssertService.removeByIds(Arrays.asList(assertIds));

        return R.ok();
    }

}
