package com.dk.api.controller;

import java.util.Arrays;
import java.util.Map;

import com.dk.api.entity.GroupEntity;
import com.dk.api.feign.EnvironmentInfoService;
import com.dk.common.utils.BaseReqDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.api.entity.ApiConfigInfoEntity;
import com.dk.api.service.ApiConfigInfoService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;



/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@RefreshScope//动态刷新配置结合@Value
@RestController
@RequestMapping("api/apiconfiginfo")
public class ApiConfigInfoController {
    @Autowired
    private ApiConfigInfoService apiConfigInfoService;
    @Autowired
    private EnvironmentInfoService environmentInfoService;


    /**
     * 查询
     */
    @RequestMapping("/list")
    //@RequiresPermissions("api:apiconfiginfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = apiConfigInfoService.queryPage(params);

        return R.ok().put("page", page);
    }
    @GetMapping("/queryApiInfoList")
    @ApiOperation(value = "查询接口信息列表",notes = "根据传参查询接口信息列表")
    public R queryApiInfoList(@RequestParam(value="apiName",required=false)String apiName,
                              @RequestParam(value="path",required=false)String path,
                              @RequestParam(value="method",required=false)Integer method,
                              @RequestParam(value="apiType",required=false)Integer apiType,
                              @RequestParam(value="page",required=true,defaultValue = "1")Integer page,
                              @RequestParam(value="limit",required=true,defaultValue = "10")Integer limit)
    {
        return apiConfigInfoService.queryApiInfoList(apiName,path,apiType,method,page,limit);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{apiId}")
    //@RequiresPermissions("api:apiconfiginfo:info")
    public R info(@PathVariable("apiId") Integer apiId){
		ApiConfigInfoEntity apiConfigInfo = apiConfigInfoService.getById(apiId);

        return R.ok().put("apiConfigInfo", apiConfigInfo);
    }

    /**
     * 保存
     */
    @PostMapping("/saveApiInfo")
    @ApiOperation(value = "修改接口组信息",notes = "根据组对象修改接口组信息")
    public R saveApiInfo(@Validated @RequestBody BaseReqDTO<ApiConfigInfoEntity> req){
        return apiConfigInfoService.saveApiInfo(req.getData());
    }

    /**
     * 修改
     */
    @PostMapping("/updateApiInfo")
    //@RequiresPermissions("api:apiconfiginfo:update")
    public R updateApiInfo(@Validated @RequestBody BaseReqDTO<ApiConfigInfoEntity> req){
        return apiConfigInfoService.updateApiInfo(req.getData());
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("api:apiconfiginfo:delete")
    public R delete(@RequestBody Integer[] apiIds){
		apiConfigInfoService.removeByIds(Arrays.asList(apiIds));

        return R.ok();
    }

}
