package com.dk.api.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dk.api.entity.CaseGroupEntity;
import com.dk.api.service.CaseGroupService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;



/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */
@RestController
@RequestMapping("api/casegroup")
public class CaseGroupController {
    @Autowired
    private CaseGroupService caseGroupService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("api:casegroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = caseGroupService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("api:casegroup:info")
    public R info(@PathVariable("id") Integer id){
		CaseGroupEntity caseGroup = caseGroupService.getById(id);

        return R.ok().put("caseGroup", caseGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("api:casegroup:save")
    public R save(@RequestBody CaseGroupEntity caseGroup){
		caseGroupService.save(caseGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("api:casegroup:update")
    public R update(@RequestBody CaseGroupEntity caseGroup){
		caseGroupService.updateById(caseGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("api:casegroup:delete")
    public R delete(@RequestBody Integer[] ids){
		caseGroupService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
