package com.dk.api.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.dk.api.entity.ApiActionExVarEntity;
import com.dk.api.entity.dto.ApiActionDTO;
import com.dk.api.entity.vo.ApiActionVO;
import com.dk.common.utils.BaseReqDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.api.entity.ApiActionEntity;
import com.dk.api.service.ApiActionService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;
/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-12 11:26:14
 */
@Api(value="动作信息管理")
@RestController
@RequestMapping("api/apiaction")
public class ApiActionController {
    @Autowired
    private ApiActionService apiActionService;

    @PostMapping("/queryActionListByParams")
    @ApiOperation(value = "查询动作列表",notes = "根据参数查询动作列表")
    public R queryActionListByParams(@Validated @RequestBody BaseReqDTO<ApiActionDTO> req){
        return apiActionService.queryActionListByParams(req.getData());
    }

    @PostMapping("/saveActionInfo")
    @ApiOperation(value = "保存动作信息",notes = "保存动作信息")
    public R saveActionInfo(@Validated @RequestBody BaseReqDTO<ApiActionVO> req){
        return apiActionService.saveActionInfo(req.getData());
    }

    @GetMapping("/info/{actionId}")
    @ApiOperation(value = "根据id获取动作信息",notes = "根据id获取动作信息")
    public R getActionInfoById(@PathVariable Integer actionId){
        return apiActionService.getActionInfoById(actionId);
    }

    @PostMapping("/updateActionInfo")
    @ApiOperation(value = "修改动作信息",notes = "修改动作信息")
    public R updateActionInfo(@RequestBody @Validated BaseReqDTO<ApiActionVO> req){
        return apiActionService.updateActionInfo(req.getData());
    }

    @PostMapping("/deleteActionInfoByIds")
    @ApiOperation(value = "根据id集合删除动作信息",notes = "根据id集合删除动作信息")
    public R deleteActionInfoByIds(@RequestBody @Validated BaseReqDTO<List<Integer>> req){
        return apiActionService.deleteActionInfoByIds(req.getData());
    }

}
