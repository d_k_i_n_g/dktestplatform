package com.dk.api.controller;

import java.util.List;

import com.dk.api.entity.dto.GroupDTO;
import com.dk.common.utils.BaseReqDTO;
import com.dk.common.utils.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.api.entity.GroupEntity;
import com.dk.api.service.GroupService;
import com.dk.common.utils.R;



/**
 * 用例组控制器
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-15 16:01:24
 */

@Api(tags = "组信息")
@RestController
@RequestMapping("api/group")
public class GroupController {
    @Autowired
    private GroupService groupService;

    /**
     * 根据传输参数查询组列表
     * @return
     */
    @PostMapping("/getCaseGroupListByParams")
    @ApiOperation(value = "根据传输参数查询组信息",notes = "根据传输参数查询组信息")
    public R getApiListByParam(@Validated @RequestBody BaseReqDTO<GroupDTO> req){
        return R.ok().put(Constant.DATA,groupService.getGroupList(req.getData()));
    }

    /**
     * 保存用例组信息
     */
    @PostMapping("/addCaseGroup")
    @ApiOperation(value = "添加用例组信息",notes = "根据传输参数添加用例组信息")
    public R addCaseGroup(@Validated @RequestBody BaseReqDTO<GroupEntity> req){
        return groupService.addCaseGroup(req.getData());
    }

    /**
     * 修改用例组信息
     */
    @PostMapping("/updateCaseGroup")
    @ApiOperation(value = "修改接口组信息",notes = "根据组对象修改接口组信息")
    public R updateCaseGroupById(@Validated @RequestBody BaseReqDTO<GroupEntity> req){
        return groupService.updateCaseGroupById(req.getData());
    }

    /**
     * 根基id数组批量删除
     */
    @PostMapping("/deleteCaseGroupByIds")
    @ApiOperation(value="批量删除指定id的组",notes = "传组id，逻辑删除")
    public R deleteGroupInId(@Validated @RequestBody BaseReqDTO<List<Integer>> req){
		groupService.deleteGroupInId(req.getData());
        return R.ok();
    }

}
