package com.dk.api;

import com.dk.api.entity.dto.ApiActionDTO;
import com.dk.api.service.ApiActionService;
import com.dk.common.utils.BaseReqDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class DkApiApplicationTests {
    @Autowired
    ApiActionService apiActionService;
    @Test
    void contextLoads() {
        BaseReqDTO<ApiActionDTO> req = new BaseReqDTO<ApiActionDTO>();
        ApiActionDTO a = new ApiActionDTO();
        a.setActionType(1);
        a.setActionName("测试");
        System.out.println(apiActionService.queryActionListByParams(a));
    }

}
