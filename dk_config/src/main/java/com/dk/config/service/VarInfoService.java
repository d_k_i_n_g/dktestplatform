package com.dk.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;
import com.dk.config.entity.VarInfoEntity;
import com.dk.config.entity.dto.VarInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-25 13:47:06
 */
public interface VarInfoService extends IService<VarInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R queryVarInfoByParams(VarInfoDTO data);

    R saveVarInfo(VarInfoEntity data);

    R updateVarInfo(VarInfoEntity data);

    R deleteVarInfoByIds(List<Integer> data);
}

