package com.dk.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;
import com.dk.config.entity.EnvironmentInfoEntity;
import com.dk.config.entity.dto.EnvironmentInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-13 09:42:17
 */
public interface EnvironmentInfoService extends IService<EnvironmentInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R queryEnvironmentInfoByParams(EnvironmentInfoDTO data);

    R saveEnvironmentInfo(EnvironmentInfoEntity data);

    R updateEnvironmentInfo(EnvironmentInfoEntity data);

    R deleteEnvironmentInfoByIds(List<Integer> data);
}

