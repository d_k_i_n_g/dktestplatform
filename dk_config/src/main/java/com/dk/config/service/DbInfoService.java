package com.dk.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;
import com.dk.config.entity.DbInfoEntity;
import com.dk.config.entity.dto.DbInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-22 16:13:22
 */
public interface DbInfoService extends IService<DbInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R queryDbInfoByParams(DbInfoDTO data);

    R saveDbInfo(DbInfoEntity data);

    R updateDbInfo(DbInfoEntity data);

    R getDbInfoById(Integer dbId);

    R deleteDbinfoByIds(List<Integer> data);

    R testDbConnect(Integer dbId);
}

