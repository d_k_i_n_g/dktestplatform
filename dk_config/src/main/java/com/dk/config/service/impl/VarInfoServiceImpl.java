package com.dk.config.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dk.common.utils.Constant;
import com.dk.common.utils.R;
import com.dk.config.entity.dto.VarInfoDTO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.config.dao.VarInfoDao;
import com.dk.config.entity.VarInfoEntity;
import com.dk.config.service.VarInfoService;


@Service("varInfoService")
public class VarInfoServiceImpl extends ServiceImpl<VarInfoDao, VarInfoEntity> implements VarInfoService {
    @Autowired
    private VarInfoDao varInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<VarInfoEntity> page = this.page(
                new Query<VarInfoEntity>().getPage(params),
                new QueryWrapper<VarInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据参数查询变量信息
     * @param data 查询参数
     * @return 查询结果
     */
    @Override
    public R queryVarInfoByParams(VarInfoDTO data) {
        Page<VarInfoEntity> page = new Page<>(data.getPage(),data.getLimit());
        QueryWrapper<VarInfoEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotBlank(data.getVarKey()),"var_key",data.getVarKey())
                .like(Strings.isNotBlank(data.getVarValue()),"var_value",data.getVarValue())
                .orderByDesc("create_time");
        IPage<VarInfoEntity> iPage = varInfoDao.selectPage(page,queryWrapper);
        return R.ok().put(Constant.PAGE,new PageUtils(iPage));
    }

    /**
     * 保存变量数据
     * @param data 变量数据
     * @return 是否保存成功
     */
    @Override
    public R saveVarInfo(VarInfoEntity data) {
        if(varInfoDao.exists(new QueryWrapper<VarInfoEntity>().eq("var_key",data.getVarKey()))){
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "变量key值已存在,请重新输入");
        }else{
            Integer num = varInfoDao.insert(data);
            if(num==0){
                return R.error(Constant.statusCode.RES_ERROR.getCode(), "保存失败,请检查数据");
            }else{
                return R.ok();
            }
        }
    }

    /**
     * 更新变量信息
     * @param data 变量数据
     * @return 是否更新成功
     */
    @Override
    public R updateVarInfo(VarInfoEntity data) {
        if(varInfoDao.exists(new QueryWrapper<VarInfoEntity>()
                .eq("var_key",data.getVarKey())
                .ne("var_id",data.getVarId())))
        {
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "变量key值已存在,请重新输入");
        }else{
            Integer num = varInfoDao.updateById(data);
            if(num==0){
                return R.error(Constant.statusCode.RES_ERROR.getCode(), "保存失败,请检查数据");
            }else{
                return R.ok();
            }
        }
    }

    /**
     * 删除变量信息
     * @param data 需要删除的变量id集合
     * @return 是否删除成功
     */
    @Override
    public R deleteVarInfoByIds(List<Integer> data) {
        varInfoDao.deleteBatchIds(data);
        return R.ok();
    }

}