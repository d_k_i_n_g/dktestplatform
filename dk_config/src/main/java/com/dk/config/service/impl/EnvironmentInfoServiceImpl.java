package com.dk.config.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dk.common.utils.Constant;
import com.dk.common.utils.R;
import com.dk.config.entity.dto.EnvironmentInfoDTO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.Query;

import com.dk.config.dao.EnvironmentInfoDao;
import com.dk.config.entity.EnvironmentInfoEntity;
import com.dk.config.service.EnvironmentInfoService;

/**
 * 环境信息相关实现
 */
@Service("environmentInfoService")
public class EnvironmentInfoServiceImpl extends ServiceImpl<EnvironmentInfoDao, EnvironmentInfoEntity> implements EnvironmentInfoService {
    @Autowired
    EnvironmentInfoDao environmentInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<EnvironmentInfoEntity> page = this.page(
                new Query<EnvironmentInfoEntity>().getPage(params),
                new QueryWrapper<EnvironmentInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据参数查询环境信息
     * @param data 查询条件
     * @return 环境信息
     */
    @Override
    public R queryEnvironmentInfoByParams(EnvironmentInfoDTO data) {
        Page<EnvironmentInfoEntity> page = new Page<EnvironmentInfoEntity>(data.getPage(), data.getLimit());
        QueryWrapper<EnvironmentInfoEntity> queryWrapper = new QueryWrapper<EnvironmentInfoEntity>();
        queryWrapper.like(Strings.isNotBlank(data.getEnvironmentName()),"environment_name",data.getEnvironmentName())
                .eq(data.getProtocol()!=0,"protocol",data.getProtocol())
                .like(Strings.isNotBlank(data.getHostname()),"hostname",data.getHostname())
                .orderByDesc("create_time");
        IPage<EnvironmentInfoEntity> iPage = page(page,queryWrapper);
        return R.ok().put(Constant.PAGE,new PageUtils(iPage));
    }

    /**
     * 保存环境信息
     * @param data 环境信息实体类
     * @return 保存结果
     */
    @Override
    public R saveEnvironmentInfo(EnvironmentInfoEntity data) {
        if(environmentInfoDao.exists(new QueryWrapper<EnvironmentInfoEntity>().eq("environment_name",data.getEnvironmentName()))){
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "环境名称已存在,请重新输入");
        }else{
            Integer num = environmentInfoDao.insert(data);
            if(num==0){
                return  R.error(Constant.statusCode.RES_ERROR.getCode(), "保存失败,请检查数据");
            }else{
                return R.ok();
            }
        }
    }

    /**
     * 修改环境信息
     * @param data 环境信息实体类
     * @return 编辑结果
     */
    @Override
    public R updateEnvironmentInfo(EnvironmentInfoEntity data) {
        if(environmentInfoDao.exists(new QueryWrapper<EnvironmentInfoEntity>().eq("environment_name",data.getEnvironmentName()).ne("id",data.getId()))){
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "环境名称已存在,请重新输入");
        }else{
            Integer num = environmentInfoDao.updateById(data);
            System.out.println("data:"+data);
            if(num==0){
                return  R.error(Constant.statusCode.RES_ERROR.getCode(), "保存失败,请检查数据");
            }else{
                return R.ok();
            }
        }
    }

    /**
     * 根据id删除环境信息
     * @param data id集合
     * @return 是否删除成功
     */
    @Override
    public R deleteEnvironmentInfoByIds(List<Integer> data) {
        environmentInfoDao.deleteBatchIds(data);
        return R.ok();
    }

}