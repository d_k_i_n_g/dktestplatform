package com.dk.config.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dk.common.exception.JdbcConnectException;
import com.dk.common.utils.*;
import com.dk.config.entity.dto.DbInfoDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.dk.config.dao.DbInfoDao;
import com.dk.config.entity.DbInfoEntity;
import com.dk.config.service.DbInfoService;


@Service("dbInfoService")
public class DbInfoServiceImpl extends ServiceImpl<DbInfoDao, DbInfoEntity> implements DbInfoService {
    @Autowired
    DbInfoDao dbInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DbInfoEntity> page = this.page(
                new Query<DbInfoEntity>().getPage(params),
                new QueryWrapper<DbInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据参数查询数据库信息
     * @param data
     * @return
     */
    @Override
    public R queryDbInfoByParams(DbInfoDTO data) {
        //获取page对象
        Page<DbInfoEntity> page = new Page<>(data.getPage(), data.getLimit());
        QueryWrapper<DbInfoEntity> queryWrapper = new QueryWrapper<DbInfoEntity>();
        queryWrapper.like(StringUtils.isNotBlank(data.getDbName()),"db_name",data.getDbName())
                .orderByDesc("create_time");
        IPage<DbInfoEntity> iPage = dbInfoDao.selectPage(page,queryWrapper);
        return R.ok().put(Constant.PAGE,new PageUtils(iPage));
    }

    /**
     * 保存数据库信息
     * @param data 数据库信息
     * @return
     */
    @Override
    public R saveDbInfo(DbInfoEntity data) {
        if(dbInfoDao.exists(new QueryWrapper<DbInfoEntity>().eq("db_name",data.getDbName()))){
            return R.error(Constant.statusCode.REQ_ERROR.getCode(),"数据库名称已存在,请重新输入");
        }else{
            Integer num = dbInfoDao.insert(data);
            if(num==1){
                return R.ok();
            }else{
                return R.error(Constant.statusCode.RES_ERROR.getCode(),"插入失败");
            }
        }

    }

    /**
     * 更新数据库信息
     * @param data 数据库信息
     * @return 是否更新成功
     */
    @Override
    public R updateDbInfo(DbInfoEntity data) {
        if(dbInfoDao.exists(new QueryWrapper<DbInfoEntity>().eq("db_name",data.getDbName()).ne("db_id",data.getDbId()))){
            return R.error(Constant.statusCode.RES_ERROR.getCode(), "数据库名称已存在,请重新输入");
        }else{
            Integer num = dbInfoDao.updateById(data);
            if(num==1){
                return R.ok();
            }else{
                return R.error(Constant.statusCode.RES_ERROR.getCode(), "更新失败");
            }
        }
    }

    /**
     * 根据id获取数据库信息
     * @param dbId 数据库id
     * @return 数据库信息
     */
    @Override
    public R getDbInfoById(Integer dbId) {
        if(dbInfoDao.exists(new QueryWrapper<DbInfoEntity>().eq("db_id",dbId))){
            DbInfoEntity dbInfoEntity = dbInfoDao.selectById(dbId);
            if(dbInfoEntity!=null){
                return R.ok().put("dbInfo", dbInfoEntity);
            }else{
                return R.error(Constant.statusCode.RES_ERROR.getCode(), "根据id查询不到数据");
            }
        }else{
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "id不存在");
        }
    }

    /**
     * 根据id删除数据库信息
     * @param data id集合
     * @return 删除是否成功
     */
    @Override
    public R deleteDbinfoByIds(List<Integer> data) {
        Integer num = dbInfoDao.deleteBatchIds(data);
        if(num>0){
            return R.ok();
        }else{
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "删除失败,请检查数据");
        }
    }

    /**
     * 测试数据库连接
     * @param dbId 数据库id
     * @return 连接结果
     */
    @Override
    public R testDbConnect(Integer dbId) {
        Connection connection = null;
        if(dbInfoDao.exists(new QueryWrapper<DbInfoEntity>().eq("db_id",dbId))){
            DbInfoEntity dbInfoEntity = dbInfoDao.selectById(dbId);
            if(dbInfoEntity!=null){
                if(dbInfoEntity.getDbUrl().contains(Constant.dbDriver.MYSQL.getDbType())){
                    connection = JdbcUtil.getConnection(Constant.dbDriver.MYSQL.getDbDriver(),dbInfoEntity.getDbUrl(),dbInfoEntity.getDbUser(),dbInfoEntity.getDbPwd());
                }
                if(connection!=null){
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        throw new JdbcConnectException("关闭连接失败,请检查数据");
                    }
                    return R.ok();
                }else{
                    return R.error(Constant.statusCode.RES_ERROR.getCode(), "连接对象获取失败,请检查数据");
                }
            }else{
                return R.error(Constant.statusCode.RES_ERROR.getCode(), "根据id查询不到数据");
            }
        }else{
            return R.error(Constant.statusCode.REQ_ERROR.getCode(), "id不存在");
        }
    }


}