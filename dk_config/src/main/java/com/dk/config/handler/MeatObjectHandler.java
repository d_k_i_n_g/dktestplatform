package com.dk.config.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * mybatis插入自动带上参数
 */
@Component
public class MeatObjectHandler implements MetaObjectHandler {

    @Override
    //使用MybatisPlus实现添加操作时，该方法会执行
    public void insertFill(MetaObject metaObject) {
        //参数：需要设置的属性；设置的时间；元数据(理解：表中的数据)
        this.strictInsertFill(metaObject,"createTime", Date.class, new Date());
        this.strictInsertFill(metaObject,"updateTime", Date.class, new Date());
        this.strictInsertFill(metaObject,"isDelete", Integer.class, 0);
    }

    @Override
    //使用MybatisPlus实现修改操作时，该方法会执行
    public void updateFill(MetaObject metaObject) {
        if(metaObject.hasSetter("update_time")){
            //参数：需要设置的属性；设置的时间；元数据(理解：表中的数据)
            this.strictInsertFill(metaObject,"updateTime", Date.class, new Date());
        }
    }
}