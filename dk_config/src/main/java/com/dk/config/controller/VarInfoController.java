package com.dk.config.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.dk.common.utils.BaseReqDTO;
import com.dk.config.entity.dto.VarInfoDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.config.entity.VarInfoEntity;
import com.dk.config.service.VarInfoService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;



/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-25 13:47:06
 */
@RestController
@RequestMapping("config/varinfo")
public class VarInfoController {
    @Autowired
    private VarInfoService varInfoService;

    @ApiOperation(value = "根据参数查询变量信息",notes = "根据参数查询变量信息")
    @PostMapping("/queryVarInfoByParams")
    public R queryVarInfoByParams(@Validated @RequestBody BaseReqDTO<VarInfoDTO> req){
        return varInfoService.queryVarInfoByParams(req.getData());
    }

    @ApiOperation(value = "保存变量信息",notes = "保存变量信息")
    @PostMapping("/saveVarInfo")
    public R saveVarInfo(@Validated @RequestBody BaseReqDTO<VarInfoEntity> req){
        return varInfoService.saveVarInfo(req.getData());
    }

    @ApiOperation(value = "修改变量信息",notes = "修改变量信息")
    @PostMapping("/updateVarInfo")
    public R updateVarInfo(@Validated @RequestBody BaseReqDTO<VarInfoEntity> req){
        return varInfoService.updateVarInfo(req.getData());
    }

    @ApiOperation(value = "根据id集合删除变量信息",notes = "根据id集合删除变量信息")
    @PostMapping("/deleteVarInfoByIds")
    public R deleteVarInfoByIds(@Validated @RequestBody BaseReqDTO<List<Integer>> req){
        return varInfoService.deleteVarInfoByIds(req.getData());
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("config:varinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = varInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{varId}")
    //@RequiresPermissions("config:varinfo:info")
    public R info(@PathVariable("varId") Integer varId){
		VarInfoEntity varInfo = varInfoService.getById(varId);

        return R.ok().put("varInfo", varInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("config:varinfo:save")
    public R save(@RequestBody VarInfoEntity varInfo){
		varInfoService.save(varInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("config:varinfo:update")
    public R update(@RequestBody VarInfoEntity varInfo){
		varInfoService.updateById(varInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("config:varinfo:delete")
    public R delete(@RequestBody Integer[] varIds){
		varInfoService.removeByIds(Arrays.asList(varIds));

        return R.ok();
    }

}
