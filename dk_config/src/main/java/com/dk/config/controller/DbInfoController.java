package com.dk.config.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.dk.common.utils.BaseReqDTO;
import com.dk.common.utils.Constant;
import com.dk.config.entity.dto.DbInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.config.entity.DbInfoEntity;
import com.dk.config.service.DbInfoService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;



/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-22 16:13:22
 */
@Api(tags = "数据库信息")
@RestController
@RequestMapping("config/dbinfo")
public class DbInfoController {
    @Autowired
    private DbInfoService dbInfoService;

    /**
     * 根据参数查询数据库信息
     * @param req 请求数据
     * @return 数据库信息列表
     */
    @ApiOperation(value="查询数据库信息",notes = "根据参数查询数据库信息")
    @PostMapping("/queryDbInfoByParams")
    public R queryDbInfoByParams(@Validated @RequestBody BaseReqDTO<DbInfoDTO> req){
        return dbInfoService.queryDbInfoByParams(req.getData());
    }

    /**
     * 保存数据库信息
     * @param req 请求信息
     * @return 是否保存成功
     */
    @ApiOperation(value = "保存数据库信息",notes = "根据传参保存数据库信息")
    @PostMapping("/saveDbInfo")
    public R saveDbInfo(@Validated @RequestBody BaseReqDTO<DbInfoEntity> req){
        return dbInfoService.saveDbInfo(req.getData());
    }

    /**
     * 更新数据库信息
     * @param req 请求数据
     * @return 是否更新成功
     */
    @ApiOperation(value = "更新数据库信息",notes = "根据传参更新数据库信息")
    @PostMapping("/updateDbInfo")
    public R updateDbInfo(@Validated @RequestBody BaseReqDTO<DbInfoEntity> req){
        return dbInfoService.updateDbInfo(req.getData());
    }

    /**
     * 根据id查询数据库信息
     */
    @ApiOperation(value = "根据id查询数据库信息",notes = "根据id查询数据库信息")
    @GetMapping("/info/{dbId}")
    public R info(@PathVariable("dbId") Integer dbId){;
        return dbInfoService.getDbInfoById(dbId);
    }

    /**
     * 根据id删除数据库信息
     */
    @ApiOperation(value = "根据id删除数据库信息",notes = "根据id删除数据库信息")
    @PostMapping("/deleteDbinfoByIds")
    //@RequiresPermissions("config:dbinfo:delete")
    public R deleteDbinfoByIds(@RequestBody BaseReqDTO<List<Integer>> req){
        return dbInfoService.deleteDbinfoByIds(req.getData());
    }

    @ApiOperation(value = "根据数据库id测试该数据库连接",notes = "根据数据库id测试该数据库连接")
    @GetMapping("/testDbConnect/{dbId}")
    public R testDbConnect(@PathVariable("dbId") Integer dbId){
        return dbInfoService.testDbConnect(dbId);
    }

}
