package com.dk.config.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.dk.common.utils.BaseReqDTO;
import com.dk.config.entity.dto.EnvironmentInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dk.config.entity.EnvironmentInfoEntity;
import com.dk.config.service.EnvironmentInfoService;
import com.dk.common.utils.PageUtils;
import com.dk.common.utils.R;



/**
 * 
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-13 09:42:17
 */
@Api("环境信息")
@RestController
@RequestMapping("config/environmentinfo")
public class EnvironmentInfoController {
    @Autowired
    private EnvironmentInfoService environmentInfoService;

    @ApiOperation(value = "根据参数查询环境信息",notes = "根据参数查询环境信息")
    @PostMapping("/queryEnvironmentInfoByParams")
    public R queryEnvironmentInfoByParams(@Validated @RequestBody BaseReqDTO<EnvironmentInfoDTO> req){
        return environmentInfoService.queryEnvironmentInfoByParams(req.getData());
    }

    @ApiOperation(value = "保存环境信息",notes = "保存环境信息")
    @PostMapping("/saveEnvironmentInfo")
    public R saveEnvironmentInfo(@Validated @RequestBody BaseReqDTO<EnvironmentInfoEntity> req){
        return environmentInfoService.saveEnvironmentInfo(req.getData());
    }

    @ApiOperation(value = "修改环境信息",notes = "修改环境信息")
    @PostMapping("/updateEnvironmentInfo")
    public R updateEnvironmentInfo(@Validated @RequestBody BaseReqDTO<EnvironmentInfoEntity> req){
        return environmentInfoService.updateEnvironmentInfo(req.getData());
    }

    @ApiOperation(value = "删除环境信息",notes = "根据id删除环境信息")
    @RequestMapping("/deleteEnvironmentInfoByIds")
    //@RequiresPermissions("config:environmentinfo:delete")
    public R deleteEnvironmentInfoByIds(@Validated @RequestBody BaseReqDTO<List<Integer>> req){
        return environmentInfoService.deleteEnvironmentInfoByIds(req.getData());
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("config:environmentinfo:info")
    public R info(@PathVariable("id") Integer id){
		EnvironmentInfoEntity environmentInfo = environmentInfoService.getById(id);

        return R.ok().put("environmentInfo", environmentInfo);
    }





}
