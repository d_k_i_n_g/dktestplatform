package com.dk.config.dao;

import com.dk.config.entity.EnvironmentInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-13 09:42:17
 */
@Mapper
public interface EnvironmentInfoDao extends BaseMapper<EnvironmentInfoEntity> {
	
}
