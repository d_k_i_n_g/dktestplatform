package com.dk.config.dao;

import com.dk.config.entity.DbInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-22 16:13:22
 */
@Mapper
public interface DbInfoDao extends BaseMapper<DbInfoEntity> {
	
}
