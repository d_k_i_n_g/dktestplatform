package com.dk.config.dao;

import com.dk.config.entity.VarInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-25 13:47:06
 */
@Mapper
public interface VarInfoDao extends BaseMapper<VarInfoEntity> {
	
}
