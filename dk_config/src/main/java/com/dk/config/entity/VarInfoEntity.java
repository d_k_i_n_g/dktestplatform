package com.dk.config.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 
 * 变量实体类
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-25 13:47:06
 */
@Data
@TableName("dk_config_var_info")
public class VarInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 变量主键
	 */

	@TableId
	@TableField("var_id")
	@ApiModelProperty(value = "变量主键",required = false,position = 1,example = "1")
	private Integer varId;
	/**
	 * 变量key值
	 */
	@Size(max = 30,message = "变量key值不得超过30位")
	@NotBlank(message = "变量key值不得为空")
	@ApiModelProperty(value = "变量key值",required = true,position = 2,example = "key")
	@TableField("var_key")
	private String varKey;

	/**
	 * 变量值
	 */
	@Size(max = 30,message = "变量value值不得超过30位")
	@NotBlank(message = "变量value值不得为空")
	@ApiModelProperty(value = "变量value值",required = true,position = 3,example = "value")
	@TableField("var_value")
	private String varValue;

	/**
	 * 备注
	 */
	@Size(max = 50,message = "备注不得超过50位")
	@ApiModelProperty(value = "备注",required = false,position = 6,example = "备注信息")
	@TableField("remark")
	private String remark;

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableLogic
	@ApiModelProperty(value = "是否删除",required = false,position = 7,example = "0")
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time",fill = FieldFill.INSERT)
	@ApiModelProperty(value = "createTime",required = false,position = 8,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;

	/**
	 * 创建人id
	 */
	@TableField("create_user_id")
	@ApiModelProperty(value = "createUserId",required = false,position = 9,example = "123456789")
	private Long createUserId;

	/**
	 * 编辑时间
	 */
	@TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
	@ApiModelProperty(value = "updateTime",required = false,position = 10,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;

	/**
	 * 编辑人id
	 */
	@TableField("update_user_id")
	@ApiModelProperty(value = "updateUserId",required = false,position = 11,example = "1")
	private Long updateUserId;

}
