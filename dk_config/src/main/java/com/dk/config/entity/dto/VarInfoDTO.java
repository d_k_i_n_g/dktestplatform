package com.dk.config.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 变量查询实体
 */
@Data
public class VarInfoDTO {
    /**
     * 变量key值
     */
    @Size(max = 30,message = "变量key值不得超过30位")
    @ApiModelProperty(value = "变量key值",required = true,position = 2,example = "key")
    private String varKey;

    /**
     * 变量值
     */
    @Size(max = 30,message = "变量value值不得超过30位")
    @ApiModelProperty(value = "变量value值",required = true,position = 3,example = "value")
    private String varValue;

    /**
     * 页码
     */
    @Max(value = 9999,message = "页码最多9999页")
    @NotNull(message = "page不得为空")
    @ApiModelProperty(value = "页码",required = true,position = 3,example = "1")
    private Integer page;

    /**
     * 查询数据量
     */
    @Max(value = 100,message = "页面数据量最多100条")
    @NotNull(message = "limit不得为空")
    @ApiModelProperty(value = "查询数据量",required = true,position = 4,example = "10")
    private Integer limit;
}
