package com.dk.config.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.dk.common.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 
 * 环境信息实体类
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-13 09:42:17
 */
@Data
@TableName("dk_config_environment_info")
public class EnvironmentInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	@TableField("id")
	@ApiModelProperty(value = "id",required = false,position = 1,example = "1")
	private Integer id;

	/**
	 * 环境名称
	 */
	@Size(max = 30,message = "环境名称不得超过30位")
	@NotBlank(message = "环境名称不能为空")
	@ApiModelProperty(value = "环境名称",required = true,position = 2,example = "测试环境")
	@TableField("environment_name")
	private String environmentName;

	/**
	 * 访问协议
	 */
	@NotNull(message = "请求协议不能为空")
	@EnumValue(intValues = {1,2},message = "请求协议枚举值错误(枚举值 1:http,2:https)")
	@ApiModelProperty(value = "请求协议",required = true,position = 3,example = "1")
	@TableField("protocol")
	private Integer protocol;

	/**
	 * 主机地址
	 */
	@Pattern(regexp = "(http://|https://)?([^/]*)",message = "主机地址格式错误,请重新输入")
	@Size(max = 200,message = "主机地址不能超过200位")
	@NotBlank(message = "主机地址不能为空")
	@ApiModelProperty(value = "主机地址",required = true,position = 4,example = "www.baidu.com")
	@TableField("hostname")
	private String hostname;

	/**
	 * 备注
	 */
	@Size(max = 50,message = "备注不得超过50位")
	@ApiModelProperty(value = "备注",required = false,position = 5,example = "备注信息")
	@TableField("remark")
	private String remark;

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableLogic
	@ApiModelProperty(value = "是否删除",required = false,position = 6,example = "0")
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time",fill = FieldFill.INSERT)
	@ApiModelProperty(value = "createTime",required = false,position = 7,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;
	/**
	 * 创建人id
	 */
	@TableField("create_user_id")
	@ApiModelProperty(value = "createUserId",required = false,position = 8,example = "123456789")
	private Long createUserId;

	/**
	 * 编辑时间
	 */
	@TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
	@ApiModelProperty(value = "updateTime",required = false,position = 9,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;

	/**
	 * 编辑人id
	 */
	@TableField("update_user_id")
	@ApiModelProperty(value = "updateUserId",required = false,position = 10,example = "1")
	private Long updateUserId;

}
