package com.dk.config.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.dk.common.annotation.EnumValue;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * 环境查询实体类
 */
@Data
public class EnvironmentInfoDTO {
    private static final long serialVersionUID = 1L;
    /**
     * 环境名称
     */
    @Size(max = 30,message = "环境名称不得超过30位")
    @ApiModelProperty(value = "环境名称",required = false,position = 2,example = "测试环境")
    @TableField("environment_name")
    private String environmentName;

    /**
     * 访问协议
     */
    @Max(value = 2,message = "请求协议不得超过2")
    @ApiModelProperty(value = "请求协议",required = false,position = 3,example = "1")
    @TableField("protocol")
    private Integer protocol;

    /**
     * 主机地址
     */
    @Size(max = 200,message = "主机地址不能超过200位")
    @ApiModelProperty(value = "主机地址",required = false,position = 4,example = "www.baidu.com")
    @TableField("hostname")
    private String hostname;

    /**
     * 页码
     */
    @Max(value = 9999,message = "页码最多9999页")
    @NotNull(message = "page不得为空")
    @ApiModelProperty(value = "页码",required = true,position = 5,example = "1")
    private Integer page;

    /**
     * 查询数据量
     */
    @Max(value = 100,message = "页面数据量最多100条")
    @NotNull(message = "limit不得为空")
    @ApiModelProperty(value = "查询数据量",required = true,position = 6,example = "10")
    private Integer limit;
}
