package com.dk.config.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 数据库信息实体类
 * 
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-22 16:13:22
 */
@Data
@TableName("dk_config_db_info")
public class DbInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 数据库主键
	 */
	@TableId
	@TableField("db_id")
	@ApiModelProperty(value = "数据库主键",required = false,position = 1,example = "1")
	private Integer dbId;
	/**
	 * 数据库名称
	 */
	@Size(max = 15,message = "数据库名称不得超过15位")
	@NotBlank(message = "数据库名称不得为空")
	@ApiModelProperty(value = "数据库名称",required = true,position = 2,example = "数据库名称")
	@TableField("db_name")
	private String dbName;
	/**
	 * 数据库url
	 */
	@Size(max = 500,message = "数据库连接地址不得超过500位")
	@NotBlank(message = "数据库连接地址不得为空")
	@ApiModelProperty(value = "数据库连接地址",required = true,position = 3,example = "jdbc:mysql://localhost:3306/dk?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai")
	@TableField("db_url")
	private String dbUrl;
	/**
	 * 数据库用户名
	 */
	@Size(max = 50,message = "数据库用户名不得超过50位")
	@NotBlank(message = "数据库用户名不得为空")
	@ApiModelProperty(value = "数据库用户名",required = true,position = 4,example = "root")
	@TableField("db_pwd")
	private String dbUser;
	/**
	 * 数据库密码
	 */
	@Size(max = 50,message = "数据库密码不得超过50位")
	@NotBlank(message = "数据库密码不得为空")
	@ApiModelProperty(value = "数据库密码",required = true,position = 5,example = "root")
	@TableField("db_user")
	private String dbPwd;
	/**
	 * 备注
	 */
	@Size(max = 50,message = "备注不得超过50位")
	@ApiModelProperty(value = "备注",required = false,position = 6,example = "备注信息")
	@TableField("remark")
	private String remark;
	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	@TableLogic
	@ApiModelProperty(value = "是否删除",required = false,position = 7,example = "0")
	@TableField(value = "is_delete",fill = FieldFill.INSERT)
	private Integer isDelete;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time",fill = FieldFill.INSERT)
	@ApiModelProperty(value = "createTime",required = false,position = 8,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;
	/**
	 * 创建人id
	 */
	@TableField("create_user_id")
	@ApiModelProperty(value = "createUserId",required = false,position = 9,example = "123456789")
	private Long createUserId;
	/**
	 * 编辑时间
	 */
	@TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
	@ApiModelProperty(value = "updateTime",required = false,position = 10,example = "2022-01-01 01:01:01")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;
	/**
	 * 编辑人id
	 */
	@TableField("update_user_id")
	@ApiModelProperty(value = "updateUserId",required = false,position = 11,example = "1")
	private Long updateUserId;

}
