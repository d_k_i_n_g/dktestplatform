package com.dk.config.entity.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * 数据库信息查询实体类
 *
 * @author lkk
 * @email 248435748@qq.com
 * @date 2022-04-22 16:13:22
 */
@Data
public class DbInfoDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 数据库名称
     */
    @Size(max = 15,message = "数据库名称不得超过15位")
    @ApiModelProperty(value = "数据库名称",required = true,position = 1,example = "数据库名称")
    @TableField("db_name")
    private String dbName;

    /**
     * 页码
     */
    @Max(value = 9999,message = "页码最多9999页")
    @NotNull(message = "page不得为空")
    @ApiModelProperty(value = "页码",required = true,position = 3,example = "1")
    private Integer page;

    /**
     * 查询数据量
     */
    @Max(value = 100,message = "页面数据量最多100条")
    @NotNull(message = "limit不得为空")
    @ApiModelProperty(value = "查询数据量",required = true,position = 4,example = "10")
    private Integer limit;

}
