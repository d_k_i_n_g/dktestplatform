package com.dk.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableFeignClients(basePackages="com.dk.config.feign")
@EnableDiscoveryClient
@EnableSwagger2
@MapperScan("com.dk.config.dao")
@SpringBootApplication
public class DkConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(DkConfigApplication.class, args);
    }

}
