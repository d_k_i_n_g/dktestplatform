package com.dk.common.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * 统一前端请求接收
 * @param <T>
 */
@Data
public class BaseReqDTO<T> {
    //秘钥
    @NotBlank(message = "accessKey不能为空")
    @ApiModelProperty(value = "秘钥",required = true,position = 1,example = "123456")
    private String accessKey;

    //主体数据
    @Valid
    @ApiModelProperty(value = "主体数据",required = true,position = 2,example = "123")
    private T data;

    //验签
    @NotBlank(message = "sign不能为空")
    @ApiModelProperty(value = "验签",required = true,position = 3,example = "&*(^&%^&UIYIUy131")
    private String sign;

    //时间戳
    @NotBlank(message = "timestamp不能为空")
    @ApiModelProperty(value = "时间戳",required = true,position = 4,example = "16598489465456")
    private String timestamp;
}
