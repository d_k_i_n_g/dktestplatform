package com.dk.common.utils;

import com.dk.common.exception.DKException;
import com.dk.common.exception.JdbcConnectException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JDBC工具类
 */
public class JdbcUtil {
    /**
     * 获取连接对象
     * @param jdbcDriver jdbc驱动名称
     * @param dbUrl 连接地址
     * @param dbUser 用户名
     * @param dbPwd 用户密码
     * @return 连接对象
     */
    public static Connection getConnection(String jdbcDriver,String dbUrl,String dbUser,String dbPwd){
        Connection conn = null;
        try {
            //注册驱动(参数为驱动名称)
            Class.forName(jdbcDriver);
            //获取连接对象
            conn = DriverManager.getConnection(dbUrl,dbUser,dbPwd);
        } catch (ClassNotFoundException e) {
            throw new JdbcConnectException("连接失败,请检查jar包版本以及驱动名称是否正确");
        } catch (SQLException e){
            throw new JdbcConnectException("连接失败,请检查配置数据");
        }
        return conn;
    }

    /**
     * 执行查询语句获取集合列表
     * @param sql 查询语句
     * @return 查询结果
     */
    public static List<Map<String,String>> select(Connection conn,String sql){
        //查询结果集
        List<Map<String,String>> result = new ArrayList<Map<String,String>>();
        try {
            //创建sql执行对象
            PreparedStatement pst = conn.prepareStatement(sql);
            //执行获取结果集
            ResultSet rs = pst.executeQuery();
            //获取元数据
            ResultSetMetaData resultSetMetaData = rs.getMetaData();
            //获取列数
            int columnCount = resultSetMetaData.getColumnCount();
            //遍历结果集
            while(rs.next()){
                Map<String,String> map = new HashMap<String,String>();
                //循环遍历行对象,获取键值对
                for(int i=1;i<=columnCount;i++){
                    //根据列数获取key
                    String key = resultSetMetaData.getColumnLabel(i);
                    //根据key获取value
                    Object value = rs.getObject(key);
                    //当value对象不为空,key-value塞入到map行对象
                    if(value!=null){
                        map.put(key,value.toString());
                    }
                }
                //行对象塞入
                result.add(map);
            }
        } catch (SQLException e) {
            throw new DKException("-----执行查询语句失败,sql:"+sql+";-----");
        }
        return result;
    }

    /**
     * 执行查询语句,获取指定字段
     * @param sql 查询语句
     * @param keys 指定key集合
     * @return 依赖变量集合
     */
    public static Map<String,String> select(Connection conn,String sql,List<String> keys){
        //结果字典
        Map<String,String> result = new HashMap<String,String>();
        //获取查询集合
        List<Map<String,String>> list = select(conn,sql);
        int k =1;
        //遍历查询列表
        for(int i=0;i<list.size();i++){
            //遍历目标key值,塞入结果字典
            for(int j=0;j<keys.size();j++){
                Map<String,String> map = list.get(i);
                String key = keys.get(j);
                if(map.containsKey(key)){
                    if(result.containsKey(key)){
                        result.put(key+"_"+String.valueOf(k),map.get(key));
                        k++;
                    }
                    result.put(key,map.get(key));
                }
            }
        }
        return result;
    }

    /**
     * 执行新增sql
     * @param sql sql语句
     * @return
     */
    public static int insert(Connection conn,String sql){
        int result = 0;
        try {
            //获取sql执行对象
            PreparedStatement pst = conn.prepareStatement(sql);
            result = pst.executeUpdate();
        } catch (SQLException e) {
            throw new DKException("---执行插入语句失败,sql:"+sql+";");
        }
        return result;
    }

    /**
     * 执行更新sql
     * @param sql 更新sql
     * @return 执行结果
     */
    public static int update(Connection conn,String sql){
        int result = 0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            result = pst.executeUpdate();
        } catch (SQLException e) {
            throw new DKException("---执行更新语句失败,sql:"+sql+";");
        }
        return result;
    }

    /**
     * 执行删除sql
     * @param sql 删除sql
     * @return 执行结果
     */
    public static int delete(Connection conn,String sql){
        int result = 0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            result = pst.executeUpdate();
        } catch (SQLException e) {
            throw new DKException("---执行删除语句失败,sql:"+sql+";");
        }
        return result;
    }
}
