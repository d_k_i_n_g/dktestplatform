package com.dk.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DkCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(DkCommonApplication.class, args);
    }

}
